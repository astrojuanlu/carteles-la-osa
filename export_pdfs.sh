#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

for file in output/*.svg; do
  inkscape $file --export-filename=${file%.*}.pdf --batch-process
done

