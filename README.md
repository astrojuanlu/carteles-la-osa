# Carteles Osa

## Notas

- código interno del producto
- nombre del producto
- "marcapáginas" 20 x 5 cm
- tipografía legible

## Uso

```
$ python fill_templates.py carteles_preview.svg products_test.csv
$ ./export_pdfs.sh
```
