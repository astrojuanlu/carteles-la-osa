"""
Script para generar los carteles de La Osa.

Basado en https://gist.github.com/astrojuanlu/2efcc28995e850d66077
"""

import argparse
import csv
import logging
from pathlib import Path
from copy import deepcopy

from lxml import etree

logger = logging.getLogger(__name__)

OUTPUT_DIR = Path("output")


def clean_name(name, stopwords=["a", "de", "y", "con"]):
    return " ".join(word for word in name.split() if word not in stopwords).strip()


class CardPrinter:
    def __init__(self, base_doc, rows, output_dir):
        self._base_doc = base_doc
        self._rows = 5
        self._output_dir = output_dir

        self._counter = 0
        self._active_doc = None

    @classmethod
    def from_template_path(cls, template_fname, *, rows=5, output_dir=OUTPUT_DIR):
        with open(template_fname, "r") as fp:
            base_doc = etree.parse(fp)

        return cls(base_doc, rows, output_dir)

    def get_or_create_active_doc(self):
        if self._active_doc is None:
            self._active_doc = deepcopy(self._base_doc)
        return self._active_doc

    def push_row(self, product_name, product_code):
        if self._counter % self._rows == 0:
            # Next row needs to be written to a different document
            self.flush()

        doc = self.get_or_create_active_doc()
        logger.info("Pushing new row (%s, %s)", product_name, product_code)
        change_data(doc, product_name, product_code, self._counter % self._rows)
        self._counter += 1

    def flush(self):
        if self._active_doc is not None:
            new_fname = f"carteles_{self._counter:05d}.svg"
            logger.info("Flushing document %s", new_fname)
            with open(self._output_dir / new_fname, "wb") as fp:
                fp.write(etree.tostring(self._active_doc, pretty_print=True))

            self._active_doc = None


def main(template_fname, products_fname):
    card_printer = CardPrinter.from_template_path(template_fname)

    with open(products_fname) as csvfile:
        reader = csv.DictReader(csvfile)
        for row_number, row in enumerate(reader):
            card_printer.push_row(clean_name(row["name"]), row["default_code"])

        # Flush at the end just in case
        card_printer.flush()


def change_data(doc, product_name, product_code, card_id):
    root = doc.getroot()
    (name_tspan,) = root.xpath(
        f"//svg:text[@id='nombre_producto_{card_id}']/svg:tspan[1]/svg:tspan[1]",
        namespaces={"svg": "http://www.w3.org/2000/svg"},
    )
    (code_tspan,) = root.xpath(
        f"//svg:text[@id='codigo_interno_{card_id}']/svg:tspan[1]/svg:tspan[1]",
        namespaces={"svg": "http://www.w3.org/2000/svg"},
    )
    name_tspan.text = product_name
    code_tspan.text = product_code
    return doc


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("template_path", help="Template (.svg) path")
    parser.add_argument("products_path", help="Products (.csv) path")
    parser.add_argument(
        "-v", "--verbose", action="store_true", help="Display debug information"
    )
    args = parser.parse_args()

    logging.basicConfig(
        level=logging.DEBUG if args.verbose else logging.INFO,
        format="[%(asctime)s] %(levelname)-4s %(name)-4s %(process)d %(funcName)-8s %(message)s",
    )

    main(
        args.template_path,
        args.products_path,
    )
